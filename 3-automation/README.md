# Practical Challenge for QA Roles at ecoPortal

## 1-planning

This folder contains test planning

## 2-execution

This folder contains executed results and test summary

## 3-automation

This folder contains automation script
To run the script, environments and tools have to be set up.
- Katalon Automation is the main tool for my work environment. However, for this test, I am going to use Selenium, Java as the main tool.
- BDD is the concept we are adopting, although this keyword (BDD) is not mentioned all the time during our work.
- Before running the script, Selenium-java library v1.19 or latest is required; Google chrome chromedriver_win32 v99.0.48 is required.
- Other libraries, such as okhttp-3.9.jar, okio-1.13.jar, are required too.
- Eclipse IDE is recommended to set up the environments and scripts for running this automation.
- Enjoy the automation test!