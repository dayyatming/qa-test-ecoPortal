package ars.news.auto.util;

import java.io.FileInputStream;
import java.util.Properties;

public class PropertyRetriever {

	public String getValue(String value){
		String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		String appConfigPath = rootPath + "Prop.properties";
		Properties appProps = new Properties();
		
		String returnValue = "";
		try{
			appProps.load(new FileInputStream(appConfigPath));
			
			switch (value) {
				case "baseUrl": return appProps.getProperty("baseUrl");
				case "logUser": return appProps.getProperty("logUser");
				case "logPass": return appProps.getProperty("logPass");
				default: return returnValue;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return returnValue;
	}	
}
