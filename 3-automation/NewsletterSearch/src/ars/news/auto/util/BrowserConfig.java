package ars.news.auto.util;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class BrowserConfig {
	
	public static WebDriver getBrowserChrome(){

		File file = new File("C:\\\\Program Files (x86)\\\\Google\\\\chromedriver_win32\\\\chromedriver.exe"); 
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath()); 
		
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		WebDriver driver = new ChromeDriver(options);
		
		return driver;
	}
	
	public static void quitBrowser(WebDriver driver){

		try {
			driver.quit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
