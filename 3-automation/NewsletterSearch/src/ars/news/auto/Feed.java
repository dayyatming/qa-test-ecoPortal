package ars.news.auto;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ars.news.auto.util.BrowserConfig;
import ars.news.auto.util.PageInfo;

public class Feed {
	public static void main(String args[]) {
		WebDriver driver = BrowserConfig.getBrowserChrome();
		System.out.println("Browser opened");

		PageInfo.getFeed(driver);
		String news = "COVID cases are again on the rise globally as testing, health measures decline";
		String titleUrl = "https://arstechnica.com/science/2022/03/covid-cases-are-again-on-the-rise-globally-as-testing-health-measures-decline/";
		
		System.out.println(news);
		System.out.println(titleUrl);
		
		try {
//			System.out.println("Locate search button");
			WebElement searchBtn = driver.findElement(By.id("header-search"));
			searchBtn.click();
			
//			System.out.println("Input text");
			WebElement searchTxt = driver.findElement(By.id("hdr_search_input"));
			searchTxt.sendKeys(news);
			searchTxt.sendKeys(Keys.ENTER);
			
			WebElement titleTxt = driver.findElement(By.xpath("//div[@id='___gcse_0']/div/div/div/div[5]/div[2]/div/div/div/div/div/div/div/a"));
			String assertTitle = titleTxt.getAttribute("href");
//			System.out.println(assertTitle);
			
			if(titleUrl.equals(assertTitle)) {
				System.out.println("News found");
			}else {
				System.out.println("News NOT found");
			}
			 
		}catch(Exception e) {
        	e.printStackTrace();
        }
		
//		BrowserConfig.quitBrowser(driver);
	}
}
